# Příklad 1
- vytvořte jednoduchý model pro auto s motorem
- vymyslete ke každé třídě vhodné atributy (stačí pár)
- navrhněte metody (kontruktor/gettery..) tak, aby bylo možné motor specifikovat POUZE při vytvoření instance auta
- ve třídě Playground v metodě cars() vytvořte 3 instance, které vytiskněte
    - vytvořte vhodně metody toString, aby se tiskly všechny informace o autě včetně informací o motoru

# Příklad 2
- vytvořte jednoduchý model pro chatovací aplikaci
- pro jednoduchost předpokládejte, že chat představuje výměnu pouze 2 zpráv, každé od jiného uživatele
- pokuste se tedy navrhnout model tříd tak, že ve finále vytisknete instanci chatu, která vytiskne 1 zprávu od uživatele 1, 1 zprávu od uživatele 2
- navrhněte třídám vhodné atributy (stačí to nejnutnější)
- ve třídě Playgroud v metodě chat() vytvořte 2 instance chatů
    - opět vytvořte vhodně toString metody
    - instance chatů budou mít plně doplěny data pro 2 zprávy od 2 uživatelů
- hint: navrhujte model tak, že se musíte být schopni dostat k informaci, jaký uživatel je autorem jaké zprávy

# Příklad 3
- abychom se naučili něco nového, vysvětlíme si další důležitý koncept = porovávání
- představte si, že se snažíte napsat aplikaci pro správu uživatelů
- pro jednoduchost (abychom nemuseli používat pole/kolekce) počítejte, že umíte spravovat 1 uživatele
- uděláte tedy model pro třídu UserManager (správcovské rozhraní) a User (představuje a drží data uživatele)
    - UserManager tedy drží našeho 1 spravovaného uživatele
- budete se snažit napsat logiku, která zabrání tomu, že přenastavíte uživatele, pokud je stejný již nastaven
- zde nám nastává problém s tím, co znamená 'stejný'?
    - do setteru uživatele v třídě UserManager budete potřebovat napsat if
        - ten říká, že pokud je nastavený uživatel rovný uživateli, kterého chceme nastavit (argument), nic se nestane
        - do ifu tedy použije == nebo !=, které jsme byli zvyklí používat k porovávání intů
        - do ifu vložte i vytisknutí 'uživatel přenastaven', ať si ověříte fungování
    - ve třídě Playground v metodě users() vytvořte instanci třídy User, nastavte ji přes setter do instance UserManager
    - toto proveďte znovu s tou stejnou instancí třídy User - výsledek by měl být, že se řetězec 'uživatel přenastaven' nevytiskne
    - vytvořte novou instanci třídy User, které dáte stejné jméno i příjmení
    - pokud tuto novou instanci pošlete do setteru, uvidíte, že se řetězec 'uživatel přenastaven' vytiskne
    - toto je demonstrace toho, že == slouží k porovnávání referencí, ovšem ne hodnot instancí
- pro řízení, co to znamená, že jsou 2 instance stejné se používá metoda equals() (stejně jako toString poděděna z třídy Object)
    - metoda dostává jako argument instanci třídy Object a musí rozhodnout, jeslti je "stejná" jako aktuální instance
    - to, jestli je stejná řídíme my tím, jakou hodnotu booleanu vrátíme, pokud true, jsou pro nás instance stejné, false=nejsou
    - napište tedy ještě 1 speciální setter setUserEquals(), který použije equals metodu, kterou napíšete do třídy User
        - napíšete ji tak, že pokud je jméno a příjmení uživatele aktuální instance stejné jako jméno a příjmení zasílaného obejktu (argument), instance považujeme za stejné
            - pro porovnávání Stringů používejte taktéž equals() metodu
            - pokud byste nevěděli jak toto napsat, equals() metoda se dá vygenerovat
        - do metody users() vyzkoušete volání metody setUserEquals, s tím, že do ní pošlete druhou instanci uživatele
            - pokud je vše správně, řetězec 'uživatel přenastaven' by se neměl vypsat
