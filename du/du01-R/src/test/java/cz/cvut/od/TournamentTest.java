package cz.cvut.od;

import cz.cvut.od.model.DefaultMatchGenerator;
import cz.cvut.od.model.Match;
import cz.cvut.od.model.Team;
import cz.cvut.od.model.Tournament;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class TournamentTest {

    @Test
    public void getTournamentWinner_validData_returnTournamentWinner(){
        Tournament tournament = new Tournament(new DefaultMatchGenerator());

        Team a = new Team("A");
        Team b = new Team("B");

        Match match = new Match(a, b);
        match.homeGoal(10);
        Match match2 = new Match(a, b);
        match2.visitorGoal(10);
        Match match3 = new Match(a, b);
        match3.homeGoal(10);

        tournament.setMatches(new ArrayList<>());
        tournament.getMatches().add(match);
        tournament.getMatches().add(match2);
        tournament.getMatches().add(match3);

        Team winner = tournament.getTournamentWinner();
        Assertions.assertEquals(a, winner);

    }
}
