package cz.cvut.od;

import cz.cvut.od.model.DefaultMatchGenerator;
import cz.cvut.od.model.Team;
import cz.cvut.od.model.Tournament;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ApplicationTest {

    @Test
    public void applicationIntegrationTest(){
        Tournament tournament = new Tournament(new DefaultMatchGenerator());
        tournament.registerTeam(new Team("Sparta"));
        tournament.registerTeam(new Team("Sigma"));
        tournament.registerTeam(new Team("Plzen"));
        tournament.registerTeam(new Team("Ostrava"));

        tournament.generateMatches();
        tournament.simulateMatches();
        Assertions.assertNotNull(tournament.getTournamentWinner());
    }
}
