package cz.cvut.od;

import cz.cvut.od.model.DefaultMatchGenerator;
import cz.cvut.od.model.Match;
import cz.cvut.od.model.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class DefaultMatchGeneratorTest {

    @Test
    public void generateMatches_lessThanTwoTeamsSent_throwException(){
        List<Team> teams = new ArrayList<>();
        DefaultMatchGenerator defaultMatchGenerator = new DefaultMatchGenerator();
        Assertions.assertThrows(IllegalStateException.class, () -> defaultMatchGenerator.generateMatches(teams));
    }

    @Test
    public void generateMatches_validNumberOfTeamsSent_matchesGenerated(){
        List<Team> teams = new ArrayList<>();
        Team a = new Team("A");
        Team b = new Team("B");
        Team c = new Team("C");
        teams.add(a);
        teams.add(b);
        teams.add(c);

        DefaultMatchGenerator defaultMatchGenerator = new DefaultMatchGenerator();
        List<Match> matches = defaultMatchGenerator.generateMatches(teams);
        Assertions.assertTrue(matches.size() > 0);
        Assertions.assertEquals(3, matches.size());

        for(Match match: matches){
            Assertions.assertFalse(match.getHomeTeam().equals(a) && match.getVisitingTeam().equals(a));
            Assertions.assertFalse(match.getHomeTeam().equals(b) && match.getVisitingTeam().equals(b));
            Assertions.assertFalse(match.getHomeTeam().equals(c) && match.getVisitingTeam().equals(c));
        }
    }
}
