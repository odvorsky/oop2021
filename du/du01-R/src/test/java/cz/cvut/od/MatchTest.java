package cz.cvut.od;

import cz.cvut.od.model.Match;
import cz.cvut.od.model.MatchResult;
import cz.cvut.od.model.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MatchTest {

    @Test
    public void getMatchResult_validData_returnRightMatchResult(){
        Team a = new Team("A");
        Team b = new Team("B");

        Match match = new Match(a, b);
        match.visitorGoal(10);
        match.visitorGoal(10);
        match.visitorGoal(10);

        match.homeGoal(10);
        match.homeGoal(10);

        MatchResult matchResult = match.getMatchResult();
        Assertions.assertEquals(2, matchResult.getHomeGoals());
        Assertions.assertEquals(3, matchResult.getVisitorGoals());
        Assertions.assertNotNull(matchResult);
    }
}
