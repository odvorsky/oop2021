package cz.cvut.od.model;

import java.util.List;

public interface MatchGenerator {

    List<Match> generateMatches(List<Team> teams);
}
