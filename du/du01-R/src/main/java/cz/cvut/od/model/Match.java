package cz.cvut.od.model;

import java.util.ArrayList;
import java.util.List;

public class Match {
    private Team homeTeam;
    private Team visitingTeam;
    private List<MatchEvent> events = new ArrayList<>();

    public Match(Team homeTeam, Team visitingTeam) {
        this.homeTeam = homeTeam;
        this.visitingTeam = visitingTeam;
    }

    public Team getWinner(){
        MatchResult matchResult = getMatchResult();
        if(matchResult.getHomeGoals() > matchResult.getVisitorGoals()){
            return homeTeam;
        }
        return visitingTeam;
    }

    public MatchResult getMatchResult(){
        int homeGoals = 0;
        int visitorGoals = 0;
        for (MatchEvent event: events){
            if(event.getType().equals(MatchEventType.GOAL)){
                if(event.getOwningTeam().equals(homeTeam)) {
                    homeGoals++;
                }
                else{
                    visitorGoals++;
                }
            }
        }
        return new MatchResult(homeGoals, visitorGoals);
    }

    public void homeGoal(int gameTime){
        events.add(new MatchEvent(MatchEventType.GOAL, gameTime, homeTeam));
    }

    public void visitorGoal(int gameTime){
        events.add(new MatchEvent(MatchEventType.GOAL, gameTime, visitingTeam));
    }

    public void startMatch(){
        events.add(new MatchEvent(MatchEventType.START, 0));
    }

    public void endMatch(){
        events.add(new MatchEvent(MatchEventType.END, 90));
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getVisitingTeam() {
        return visitingTeam;
    }

    public void setVisitingTeam(Team visitingTeam) {
        this.visitingTeam = visitingTeam;
    }

    @Override
    public String toString() {
        return "Match{" +
                "homeTeam=" + homeTeam +
                ", visitingTeam=" + visitingTeam +
                '}';
    }
}
