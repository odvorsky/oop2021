package cz.cvut.od.model;

public enum MatchEventType {
    START,
    END,
    GOAL
}
