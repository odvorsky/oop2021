package cz.cvut.od.model;

import java.util.ArrayList;
import java.util.List;

public class DefaultMatchGenerator implements MatchGenerator {

    @Override
    public List<Match> generateMatches(List<Team> teams) {
        if(teams.size() < 2){
            throw new IllegalStateException("not enough teams registered");
        }
        List<Match> matches = new ArrayList<>();
        for (int i = 0; i < teams.size(); i++) {
            for (int j = i + 1; j < teams.size(); j++) {
                Team home = teams.get(i);
                Team visitor = teams.get(j);
                matches.add(new Match(home, visitor));
            }
        }
        return matches;
    }
}
