package cz.cvut.od.model;

import java.util.*;

public class Tournament {

    private List<Team> teams = new ArrayList<>();
    private MatchGenerator matchGenerator;
    private List<Match> matches;

    public Tournament(MatchGenerator matchGenerator) {
        this.matchGenerator = matchGenerator;
    }

    public Team getTournamentWinner() {
        Map<Team, Integer> resultsMap = new HashMap<>();
        for (Match match : matches) {
            Team winner = match.getWinner();
            if (resultsMap.containsKey(winner)) {
                resultsMap.put(winner, resultsMap.get(winner) + 1);
            } else {
                resultsMap.put(winner, 1);
            }
        }
        int max = Collections.max(resultsMap.values());
        for (Map.Entry<Team, Integer> entry : resultsMap.entrySet()) {
            if (entry.getValue() == max) {
                return entry.getKey();
            }
        }
        return null;
    }

    public void simulateMatches() {
        for (Match match : matches) {
            simulateMatch(match);
        }
    }

    public void simulateMatch(Match match) {
        match.startMatch();
        Random random = new Random();
        int homeGoalEvents = random.nextInt(5);
        int visitorGoalEvents = random.nextInt(5);
        for (int i = 0; i < homeGoalEvents; i++) {
            match.homeGoal(10);
        }
        for (int i = 0; i < visitorGoalEvents; i++) {
            match.visitorGoal(10);
        }
        match.endMatch();
    }

    public void registerTeam(Team team) {
        teams.add(team);
    }

    public void generateMatches() {
        this.matches = matchGenerator.generateMatches(this.teams);
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }

    @Override
    public String toString() {
        return "Tournament{" +
                "teams=" + teams +
                ", matchGenerator=" + matchGenerator +
                ", matches=" + matches +
                '}';
    }
}
