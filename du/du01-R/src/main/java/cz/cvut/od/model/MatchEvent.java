package cz.cvut.od.model;

import java.time.ZonedDateTime;

public class MatchEvent {

    private MatchEventType type;
    private ZonedDateTime time = ZonedDateTime.now();
    private int gameTime;
    private Team owningTeam;

    public MatchEvent(MatchEventType type, int gameTime) {
        this.type = type;
        this.gameTime = gameTime;
    }

    public MatchEvent(MatchEventType type, int gameTime, Team owningTeam) {
        this.type = type;
        this.gameTime = gameTime;
        this.owningTeam = owningTeam;
    }

    public MatchEventType getType() {
        return type;
    }

    public void setType(MatchEventType type) {
        this.type = type;
    }

    public Team getOwningTeam() {
        return owningTeam;
    }

    public void setOwningTeam(Team owningTeam) {
        this.owningTeam = owningTeam;
    }

    public int getGameTime() {
        return gameTime;
    }

    public void setGameTime(int gameTime) {
        this.gameTime = gameTime;
    }

    public ZonedDateTime getTime() {
        return time;
    }

    public void setTime(ZonedDateTime time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "MatchEvent{" +
                "type=" + type +
                ", time=" + time +
                ", gameTime=" + gameTime +
                ", owningTeam=" + owningTeam +
                '}';
    }
}
