package cz.cvut.od.model;

public class MatchResult {
    private int homeGoals;
    private int visitorGoals;

    public MatchResult(int homeGoals, int visitorGoals) {
        this.homeGoals = homeGoals;
        this.visitorGoals = visitorGoals;
    }

    public int getHomeGoals() {
        return homeGoals;
    }

    public void setHomeGoals(int homeGoals) {
        this.homeGoals = homeGoals;
    }

    public int getVisitorGoals() {
        return visitorGoals;
    }

    public void setVisitorGoals(int visitorGoals) {
        this.visitorGoals = visitorGoals;
    }

    @Override
    public String toString() {
        return homeGoals + ":" + visitorGoals;
    }
}
