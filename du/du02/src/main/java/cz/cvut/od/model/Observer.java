package cz.cvut.od.model;

public interface Observer<T> {

    void onMessage(T message);

}
