package cz.cvut.od;

import cz.cvut.od.model.Group;
import cz.cvut.od.model.User;

public class Application {

    public static void main(String[] args) {
        User u1 = new User("u1");
        User u2 = new User("u2");
        User u3 = new User("u3");
        User u4 = new User("u4");

        Group koc = new Group("Kočičkáři");
        Group pejs = new Group("Pejskaři");

        u1.addFriend(u2);
        u1.addFriend(u3);
        u4.addFriend(u4);

        u2.addFriend(u3);
        u2.addFriend(u4);

        pejs.addFriend(u3);
        pejs.addFriend(u4);

        koc.addFriend(u1);
        koc.addFriend(u2);

        u3.addPost("mám se dnes super!");
        pejs.addPost(u3, "mám rád pejsky kámoši");

        u1.addPost("dnes je den pod psa");
        koc.addPost(u2, "kočky jsou lepší jak psi!");

        System.out.println(u1);
        System.out.println(u2);
        System.out.println(u3);
        System.out.println(u4);


    }
}
