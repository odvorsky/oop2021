package cz.cvut.od.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class User implements Observer<Post>, Observable<Post> {

    private String username;
    private List<User> friends = new ArrayList<>();
    private List<Post> profilePosts = new ArrayList<>();
    private Feed feed = new Feed();
    private List<Group> groups = new ArrayList<>();

    public User(String username) {
        this.username = username;
    }

    public void addPost(String text){
        Post post = new Post(this, text);
        this.profilePosts.add(post);
        this.notifyFriends(post);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(username, user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }

    public List<Post> getProfilePosts() {
        return profilePosts;
    }

    public void setProfilePosts(List<Post> profilePosts) {
        this.profilePosts = profilePosts;
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    @Override
    public void onMessage(Post post) {
        this.feed.addPost(post);
    }

    @Override
    public void addFriend(User user) {
        this.friends.add(user);
        user.getFriends().add(this);
    }

    @Override
    public void notifyFriends(Post post) {
        this.friends.forEach(user -> user.onMessage(post));
    }

    @Override
    public String toString() {
        return "username=" + username +
                ", feed=" + feed;
    }
}
