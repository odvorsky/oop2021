package cz.cvut.od.model;

import java.util.Objects;

public class Post {

    private User author;
    private Group group;
    private String text;

    public Post(User author, String text) {
        this.author = author;
        this.text = text;
    }

    public Post(User author, Group group, String text) {
        this.author = author;
        this.group = group;
        this.text = text;
    }


    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return Objects.equals(author, post.author) && Objects.equals(text, post.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(author, text);
    }

    @Override
    public String toString() {
        return "Post{" +
                "author=" + author.getUsername() +
                ", group=" + (group != null ? group.getName() : "none") +
                ", text='" + text + '\'' +
                '}';
    }
}
