package cz.cvut.od.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Group implements Observable<Post>{
    private String name;
    private List<User> users = new ArrayList<>();
    private List<Post> posts = new ArrayList<>();

    public Group(String name) {
        this.name = name;
    }

    public void addPost(User user, String text){
        Post post = new Post(user, this, text);
        this.posts.add(post);
        this.notifyFriends(post);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(name, group.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    @Override
    public void addFriend(User user) {
        this.users.add(user);
        user.getGroups().add(this);
    }

    @Override
    public void notifyFriends(Post post) {
        this.users.forEach(user -> user.onMessage(post));
    }
}
