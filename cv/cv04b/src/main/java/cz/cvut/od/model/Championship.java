package cz.cvut.od.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Championship {

    List<Team> teams = new ArrayList<>();
    List<Match> matches = new ArrayList<>();

    public void generateMatches(){
        for(int i = 0; i < teams.size(); i++){
            Team home = teams.get(i);
            for(int j = i + 1; j < teams.size(); j++){
                Team out = teams.get(j);
                matches.add(new Match(home, out));
            }
        }
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void registerTeam(Team team){
        teams.add(team);
    }
}
