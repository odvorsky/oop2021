package cz.cvut.od.model;

public class Patient {

    private String name;
    private int age;
    private Illness illness;

    public Patient() {
    }

    public Patient(String name, int age){
        this.name = name;
        this.age = age;
    }

    public Patient(String name, int age, Illness illness) {
        this.name = name;
        this.age = age;
        this.illness = illness;
    }

    public boolean isGonnaDie(){
        if(this.illness != null){
            Illness illness = this.illness;
            return illness.isDeadly();
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Illness getIllness() {
        return illness;
    }

    public void setIllness(Illness illness) {
        this.illness = illness;
    }
}
