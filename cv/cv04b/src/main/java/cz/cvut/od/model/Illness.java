package cz.cvut.od.model;

public class Illness {

    private String name;
    private boolean deadly;

    public Illness(String name, boolean deadly) {
        this.name = name;
        this.deadly = deadly;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDeadly() {
        return deadly;
    }

    public void setDeadly(boolean deadly) {
        this.deadly = deadly;
    }
}
