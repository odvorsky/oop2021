package cz.cvut.od.model;

public class Match {
    private Team teamOne;
    private Team teamTwo;

    public Match() {
    }

    public Match(Team teamOne, Team teamTwo) {
        this.teamOne = teamOne;
        this.teamTwo = teamTwo;
    }

    public Team getTeamOne() {
        return teamOne;
    }

    public void setTeamOne(Team teamOne) {
        this.teamOne = teamOne;
    }

    public Team getTeamTwo() {
        return teamTwo;
    }

    public void setTeamTwo(Team teamTwo) {
        this.teamTwo = teamTwo;
    }

    @Override
    public String toString() {
        return "Zápas: " + this.teamOne + " X " + this.teamTwo;
    }
}
