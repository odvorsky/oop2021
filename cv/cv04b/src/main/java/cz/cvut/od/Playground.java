package cz.cvut.od;

import cz.cvut.od.model.*;

public class Playground {

    public void play() {
        matches();
        //generate();
    }

    public void matches() {
        Team slavia = new Team("Slavia");
        Team sparta = new Team("Sparta");
        Team olomouc = new Team("Olomouc");
        Team jablonec = new Team("Jablonec");

        Match match = new Match(slavia, sparta);
        Match match1 = new Match(slavia, olomouc);
        Match match2 = new Match(jablonec, olomouc);

        System.out.println(slavia);
        System.out.println(match1);
    }

    public void generate() {
        Championship championship = new Championship();
        championship.registerTeam(new Team("Sparta"));
        championship.registerTeam(new Team("Slavia"));
        championship.registerTeam(new Team("Sigma Olomouc"));
        championship.registerTeam(new Team("Jablonec"));
        championship.generateMatches();
        System.out.println(championship.getMatches());
    }

    public void patients() {
        Patient patient = new Patient();
        patient.setAge(10);
        patient.setName("Adam");

        Patient patient2 = new Patient("David", 20);

        Illness illness1 = new Illness("angina", false);

        Illness illness2 = new Illness("angina", false);

        patient.setIllness(illness1);
        patient2.setIllness(illness2);

        illness1.setName("smrtelna angina");

        System.out.println("Pacient jedna umre? " + patient.getIllness().isDeadly());
        System.out.println("Pacient jedna umre? " + patient.isGonnaDie());

        Patient patient3 = new Patient("Michal", 15);
        System.out.println("Pacient tri umre? " + patient3.isGonnaDie());
    }
}
