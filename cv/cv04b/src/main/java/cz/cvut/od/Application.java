package cz.cvut.od;

public class Application {

    public static void main(String[] args) {
        Playground playground = new Playground();
        playground.play();
    }
}
