package cz.cvut.od.model;

public class Game {

    private Hero hero;
    private Room currentRoom;

    public Game(){

        Room jeskyne = new Room("jeskyne", "hluboka jeskyne");
        Room vodopad = new Room("vodopad", "blabla");
        Room chata = new Room("chata", "blabla");

        vodopad.getExits().add(chata);
        jeskyne.getExits().add(vodopad);

        Item item = new Item("houba", true);

        jeskyne.getItems().add(item);

        this.hero = new Hero("adam", 50);
        this.currentRoom = jeskyne;
    }
}
