package cz.cvut.od;

import cz.cvut.od.dp.CommandLineUi;
import cz.cvut.od.dp.Item;
import cz.cvut.od.dp.ItemBuilder;
import cz.cvut.od.model.Game;

public class Application {
    public static void main(String[] args) {
        //new Game();
        // TODO, getInstance musí vrátit vždy tu stejnou instanci
        CommandLineUi commandLineUi = CommandLineUi.getInstance();
        CommandLineUi commandLineUi2 = CommandLineUi.getInstance();

        if(commandLineUi == commandLineUi2){
            System.out.println("je singleton");
        }

        Item item = new ItemBuilder()
                .name("meč")
                .description("blabla")
                .build();

        Item item2 = new ItemBuilder()
                .name("meč")
                .description("blabla")
                .build();

        Item item3 = new ItemBuilder().build();
    }
}
