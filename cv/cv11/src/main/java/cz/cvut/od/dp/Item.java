package cz.cvut.od.dp;

public class Item {
    private String name;
    private String description;

    public Item(ItemBuilder itemBuilder){
        this.name = itemBuilder.getName();
        this.description = itemBuilder.getDescription();
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
