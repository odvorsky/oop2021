package cz.cvut.od.model;

public class Item {
    private String name;
    private boolean pickable;

    public Item(String name, boolean pickable) {
        this.name = name;
        this.pickable = pickable;
    }
}
