package cz.cvut.od.dp;

public class CommandLineUi {

    private static CommandLineUi instance;

    private CommandLineUi() {

    }

    public static CommandLineUi getInstance() {
        if (instance == null) {
            instance = new CommandLineUi();
        }
        return instance;
    }
}
