package cz.cvut.od.model;

public class Weapon extends Item {
    private int minDmg;
    private int maxDmg;

    public Weapon(String name, boolean pickable, int minDmg, int maxDmg) {
        super(name, pickable);
        this.minDmg = minDmg;
        this.maxDmg = maxDmg;
    }

    public int getMinDmg() {
        return minDmg;
    }

    public void setMinDmg(int minDmg) {
        this.minDmg = minDmg;
    }

    public int getMaxDmg() {
        return maxDmg;
    }

    public void setMaxDmg(int maxDmg) {
        this.maxDmg = maxDmg;
    }
}
