package cz.cvut.od.model;

import java.util.ArrayList;
import java.util.List;

public class Chest extends Item{

    private List<Item> items = new ArrayList<>();

    public Chest(String name, boolean pickable) {
        super(name, pickable);
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
