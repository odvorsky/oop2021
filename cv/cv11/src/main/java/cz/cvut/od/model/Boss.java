package cz.cvut.od.model;

public class Boss extends Enemy {

    public Boss(String name, int hp, int dmg) {
        super(name, hp, dmg);
    }
}
