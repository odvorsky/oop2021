package playground.model;

public class Person {

    private Illness illness;


    public void setIllness(Illness illness){
        this.illness = illness;
    }

    public boolean isGonnaDie(){
        return this.illness.isDeadly();
    }
}
