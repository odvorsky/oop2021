
public class Application {


    public static void main(String[] args) {

        int myFirstProperty;

        //zapamatovat
        int a = 5;
        int b = 5;
        long c = 5;
        boolean f = true;
        String name = "Honza";

        double d = 0.0;
        float e = 0.0f;
        char g = 'a';

        int result = a + b;

        String resultText = "Výsledek: ";
        System.out.println(resultText + result);

        System.out.println("Výsledek: " + result);
        result = 5 + 5;
        System.out.println(result);
        System.out.println(5 + 5);

        if(true){

        }

        if(false){

        }

        if(!false){

        }

        if((result == 10 && result < 20) || result > 1){
            System.out.println("vysledek je 10");
        } else if(true){

        }else {

        }

        if(!(result == 10) || result != 10){

        }

        if(result == 10){
            if(result < 20){
                System.out.println("vysledek je 10");
            }
        }

        int[] myFirstArray = { 1, 2, 3, 4, 5 };

        System.out.println(myFirstArray[2]);
        myFirstArray[2] = 99;
        System.out.println(myFirstArray[2]);

        // inicializuj prazdne pole s delkou 5 prvku
        int[] mySecondArray = new int[5];
        mySecondArray[0] = 1;

        int[] myThirdArray = { 1, 2, 3, 4, 5, 6, 7, 8 };

        for(int val: myThirdArray){
            System.out.println(val);
        }

        int num = 1;
        num++;
        System.out.println(num);

        for(int i = 0; i < myThirdArray.length; i++){
            System.out.println(myThirdArray[i]);
        }
    }
}
