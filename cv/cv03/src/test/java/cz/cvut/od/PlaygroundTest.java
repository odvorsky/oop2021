package cz.cvut.od;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PlaygroundTest {

    @Test
    public void play_AStringSupplied_returnTrue(){
        Playground playground = new Playground();
        boolean result = playground.play("A");
        Assertions.assertTrue(result);
    }

    @Test
    public void play_AStringSupplied_returnFalse(){
        Playground playground = new Playground();
        boolean result = playground.play("B");
        Assertions.assertFalse(result);
    }
}
