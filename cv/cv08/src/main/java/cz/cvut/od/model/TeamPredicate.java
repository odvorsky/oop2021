package cz.cvut.od.model;

public interface TeamPredicate {

    boolean test(Team team);

}
