package cz.cvut.od;

import cz.cvut.od.model.NameWithBTeamPredicate;
import cz.cvut.od.model.ScoreMoreThanThreeTeamPredicate;
import cz.cvut.od.model.Team;
import cz.cvut.od.model.TeamPredicate;

import java.util.ArrayList;
import java.util.List;

public class Playground {

    public void play(){
        List<Team> teams = new ArrayList<>();
        teams.add(new Team("A", 2));
        teams.add(new Team("B", 3));
        teams.add(new Team("C", 100));

        TeamPredicate teamPredicate = new ScoreMoreThanThreeTeamPredicate();
        TeamPredicate namePredicate = new TeamPredicate() {
            @Override
            public boolean test(Team team) {
                return team.getName().startsWith("B");
            }
        };

        TeamPredicate nameAPredicate = team -> team.getName().startsWith("A");

        System.out.println(filterTeams(teams, teamPredicate));
        System.out.println(filterTeams(teams, namePredicate));
        System.out.println(filterTeams(teams, team -> team.getName().equals("aaaa")));
    }

    public List<Team> filterTeams(List<Team> teams, TeamPredicate predicate){
        List<Team> result = new ArrayList<>();
        for(Team team: teams){
            if(predicate.test(team)){
                result.add(team);
            }
        }
        return result;
    }
}
