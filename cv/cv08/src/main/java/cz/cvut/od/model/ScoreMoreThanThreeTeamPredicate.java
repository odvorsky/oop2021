package cz.cvut.od.model;

public class ScoreMoreThanThreeTeamPredicate implements TeamPredicate{

    @Override
    public boolean test(Team team) {
        return team.getScore() > 3;
    }
}
