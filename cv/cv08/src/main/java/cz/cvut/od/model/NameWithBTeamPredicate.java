package cz.cvut.od.model;

public class NameWithBTeamPredicate implements TeamPredicate{

    @Override
    public boolean test(Team team) {
        return team.getName().startsWith("B");
    }
}
