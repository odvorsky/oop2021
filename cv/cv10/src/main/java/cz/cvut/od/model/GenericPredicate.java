package cz.cvut.od.model;

public interface GenericPredicate<TYPE> {

    boolean test(TYPE value);
}
