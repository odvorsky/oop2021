package cz.cvut.od.model;

public class Team {

    private String homeTown;

    public Team(String homeTown) {
        this.homeTown = homeTown;
    }

    public String getHomeTown() {
        return homeTown;
    }

    public void setHomeTown(String homeTown) {
        this.homeTown = homeTown;
    }

    @Override
    public String toString() {
        return "Team{" +
                "homeTown='" + homeTown + '\'' +
                '}';
    }
}
