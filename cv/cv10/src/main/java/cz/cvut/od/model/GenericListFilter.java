package cz.cvut.od.model;

import java.util.List;

public class GenericListFilter<TYPE> {

    public int filterList(List<TYPE> values, GenericPredicate<TYPE> predicate){
        int count = 0;
        for(TYPE value: values){
            if(predicate.test(value)){
                count++;
            }
        }
        return count;
    }
}
