package cz.cvut.od;

import cz.cvut.od.model.Animal;
import cz.cvut.od.model.GenericListFilter;
import cz.cvut.od.model.GenericPredicate;
import cz.cvut.od.model.Team;

import java.util.ArrayList;
import java.util.List;

public class Playground {

    public void play() {
        // krok 1. vytvořit generický predikátový interface
        // krok 2. udělat/zkopírovat si modelovou třídu pro Team
        // krok 3. udělat/zkopírovat si metodu filterList
        // krok 4. metoda musí být s generikou,
            // musí v ní být zacvaknutý náš generický predikát
        // krok 5. vyzkoušet na nějakých datech
        // krok 6. upravte filterList aby vracela výsledky, které splňují predikát


        List<Team> teams = new ArrayList<>();
        teams.add(new Team("Praha"));
        teams.add(new Team("Olomouc"));
        teams.add(new Team("Aš"));
        GenericPredicate<Team> teamPredicate = value -> value.getHomeTown().length() > 4;
        GenericPredicate<Animal> animalPredicate = value -> true;

        GenericPredicate<Team> teamPredAc = new GenericPredicate<Team>() {
            @Override
            public boolean test(Team value) {
                return value.getHomeTown().length() > 4;
            }
        };

        List<Team> result = filterList(teams, teamPredAc);
        System.out.println(result);

        List<Animal> animals = new ArrayList<>();
        animals.add(new Animal("Ptak"));
        animals.add(new Animal("Kocka"));
        animals.add(new Animal("Pes"));

        List<Animal> resultAnimals = filterList(animals, value -> value.getName().equals("Pes"));

        // GenericListFilter<Team> filter = new GenericListFilter<>();
        // result = filter.filterList(teams, teamPredicate);
        // System.out.println(result);

    }
    // krok 6. upravte filterList aby vracela výsledky, které splňují predikát
    public <TYPE> List<TYPE> filterList(List<TYPE> values, GenericPredicate<TYPE> predicate){
        List<TYPE> results = new ArrayList<>();
        for(TYPE value: values){
            if(predicate.test(value)){
                results.add(value);
            }
        }
        return results;
    }

    public void genericPredicate(){
        GenericPredicate<Team> teamPredicate = (Team team) -> team.equals("1");

        GenericPredicate<Team> teamPredicateAc = new GenericPredicate<Team>() {
            @Override
            public boolean test(Team value) {
                return value.equals("1");
            }
        };

        //boolean result = teamPredicate.test(new Team());
        //boolean result2 = teamPredicateAc.test(new Team());

        GenericPredicate<Animal> animalPredicate = (Animal animal) -> animal.equals("1");
    }


    public <TYPE> TYPE testGenerika(TYPE input, List<TYPE> inputs){
        for(TYPE in: inputs){
            return in;
        }
        return null;
    }
}
