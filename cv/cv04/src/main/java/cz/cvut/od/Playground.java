package cz.cvut.od;

import cz.cvut.od.model.Animal;
import cz.cvut.od.model.Dog;

public class Playground {

    public void play(){
        animals();
    }

    public void animals(){
        Animal animal = new Animal();
        animal.makeSound();

        Dog dog = new Dog();
        dog.makeSound();
        dog.bite();

        Animal dog2 = new Dog();
        dog2.makeSound();

        makeSound(dog);
        makeSound(dog2);
        makeSound(animal);
    }

    public void makeSound(Animal animal){
        animal.makeSound();
    }

}
