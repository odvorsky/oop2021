package cz.cvut.od.model;

public class Dog extends Animal {

    public Dog(){
        System.out.println("DOG CONSTRUCTOR CALLED.");
    }

    public void bite(){
        System.out.println("dog bitten postman");
    }

    @Override
    public void makeSound() {
        super.makeSound();
    }
}
