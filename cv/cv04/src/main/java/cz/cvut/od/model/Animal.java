package cz.cvut.od.model;

public class Animal {

    public Animal(){
        System.out.println("ANIMAL CONSTRUCTOR CALLED");
    }

    public void makeSound(){
        System.out.println("VRRRRR");
    }
}
