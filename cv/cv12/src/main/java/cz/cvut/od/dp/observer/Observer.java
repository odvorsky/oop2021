package cz.cvut.od.dp.observer;

public interface Observer<T> {

    void onData(T data);
}
