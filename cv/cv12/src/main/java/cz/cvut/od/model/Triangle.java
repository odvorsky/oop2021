package cz.cvut.od.model;

public class Triangle implements Polygon{
    @Override
    public int getNumberOfSides() {
        return 3;
    }
}
