package cz.cvut.od.model;

import cz.cvut.od.dp.observer.Observer;

public class Person implements Observer<String> {
    private String name;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void hotdogReady() {
        System.out.println("got info that hotdog is ready for: " + this.getName());
    }

    @Override
    public void onData(String data) {
        System.out.println("got info that hotdog is ready for: " + this.getName());
        System.out.println(data);
    }
}
