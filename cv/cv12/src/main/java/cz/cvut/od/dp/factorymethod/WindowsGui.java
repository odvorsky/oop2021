package cz.cvut.od.dp.factorymethod;

import cz.cvut.od.model.Button;
import cz.cvut.od.model.WindowsButton;

public class WindowsGui extends AbstractGui{

    @Override
    public Button createButton() {
        return new WindowsButton();
    }
}
