package cz.cvut.od.dp.observer;

import java.util.ArrayList;
import java.util.List;

public class Observable<T> {

    private List<Observer<T>> observers = new ArrayList<>();

    public void registerObserver(Observer<T> observer){
        this.observers.add(observer);
    }

    public void notifyAllObservers(T data){
        for(Observer<T> observer: observers){
            observer.onData(data);
        }
    }

}
