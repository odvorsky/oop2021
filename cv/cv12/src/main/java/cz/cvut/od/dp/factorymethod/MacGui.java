package cz.cvut.od.dp.factorymethod;

import cz.cvut.od.model.Button;
import cz.cvut.od.model.MacButton;

public class MacGui extends AbstractGui{

    @Override
    public Button createButton() {
        return new MacButton();
    }
}
