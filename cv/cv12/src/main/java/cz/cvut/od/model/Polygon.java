package cz.cvut.od.model;

public interface Polygon {

    int getNumberOfSides();
}
