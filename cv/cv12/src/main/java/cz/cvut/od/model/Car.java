package cz.cvut.od.model;

import cz.cvut.od.dp.flyweight.CarFlyweight;

public class Car {
    private CarFlyweight carFlyweight;
    private String color;

    public Car(CarFlyweight carFlyweight, String color) {
        this.carFlyweight = carFlyweight;
        this.color = color;
    }

    public CarFlyweight getCarFlyweight() {
        return carFlyweight;
    }

    public void setCarFlyweight(CarFlyweight carFlyweight) {
        this.carFlyweight = carFlyweight;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carFlyweight=" + carFlyweight +
                ", color='" + color + '\'' +
                '}';
    }
}
