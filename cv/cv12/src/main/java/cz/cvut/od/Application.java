package cz.cvut.od;

public class Application {
    public static void main(String[] args) throws InterruptedException {
        new Playground().play();
    }
}
