package cz.cvut.od.dp.factorymethod;

import cz.cvut.od.model.Button;

public abstract class AbstractGui {

    public void show(){
        System.out.println("gui was shown");
        Button button = createButton();
        button.click();
    }

    public abstract Button createButton();

}
