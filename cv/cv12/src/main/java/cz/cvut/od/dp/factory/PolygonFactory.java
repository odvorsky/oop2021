package cz.cvut.od.dp.factory;

import cz.cvut.od.model.Polygon;
import cz.cvut.od.model.Square;
import cz.cvut.od.model.Triangle;

public class PolygonFactory {

    public Polygon createPolygon(int numOfSides) {
        if (numOfSides == 3) {
            return new Triangle();
        } else if (numOfSides == 4) {
            return new Square();
        }
        throw new IllegalStateException("unsupported number of sides provided");
    }
}
