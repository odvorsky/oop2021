package cz.cvut.od.dp.proxy;

import cz.cvut.od.model.Gate;
import cz.cvut.od.model.Person;

public class GateLoggingProxy implements Gate {

    private Gate gate;

    public GateLoggingProxy(Gate gate) {
        this.gate = gate;
    }

    @Override
    public void open(Person person) {
        System.out.println("person: " + person.getName() + " opened gate");
        this.gate.open(person);
    }
}
