package cz.cvut.od.model;

import cz.cvut.od.dp.observer.Observable;

public class HotdogStand extends Observable<String> {

    public void makeOrder(Person person) {
        this.registerObserver(person);
    }

    public void hotDogReady() {
        this.notifyAllObservers("hotdog ready");
    }
}
