package cz.cvut.od.model;

public interface Gate {

    void open(Person person);

}
