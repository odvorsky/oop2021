package cz.cvut.od.model;

public interface Button {

    void click();

}
