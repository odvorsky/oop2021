package cz.cvut.od;

import cz.cvut.od.dp.factory.PolygonFactory;
import cz.cvut.od.dp.factorymethod.AbstractGui;
import cz.cvut.od.dp.factorymethod.MacGui;
import cz.cvut.od.dp.factorymethod.WindowsGui;
import cz.cvut.od.dp.flyweight.CarFlyweight;
import cz.cvut.od.dp.proxy.GateLoggingProxy;
import cz.cvut.od.model.*;

public class Playground {

    public void play() throws InterruptedException {
        //factory();
        //factoryMethod();
        //flyweight();
        //proxy();
        observer();
    }

    public void observer() throws InterruptedException {

        Person honza = new Person("Honza");
        Person david = new Person("David");
        HotdogStand hotdogStand = new HotdogStand();
        hotdogStand.makeOrder(honza);
        hotdogStand.makeOrder(david);

        System.out.println("hotdogs are being made...");
        Thread.sleep(3000L);
        hotdogStand.hotDogReady();
    }

    public void proxy(){
        Person person = new Person("Honza");
        Gate gate = new EntryGate();
        gate.open(person);

        gate = new GateLoggingProxy(gate);
        gate.open(person);


    }

    public void flyweight(){
        CarFlyweight octavia = new CarFlyweight("skoda", "octavia");

        Car car = new Car(octavia, "black");
        Car car2 = new Car(octavia, "blue");
        Car car3 = new Car(octavia, "white");

        System.out.println(car);
        System.out.println(car2);
        System.out.println(car3);
    }

    public void factoryMethod(){
        AbstractGui winGui = new WindowsGui();
        winGui.show();

        AbstractGui macGui = new MacGui();
        macGui.show();
    }

    public void factory(){
        PolygonFactory polygonFactory = new PolygonFactory();
        Polygon triangle = polygonFactory.createPolygon(3);
        System.out.println(triangle.getNumberOfSides());

        Polygon square = polygonFactory.createPolygon(4);
        System.out.println(square.getNumberOfSides());
    }
}
