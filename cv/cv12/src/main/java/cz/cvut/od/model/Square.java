package cz.cvut.od.model;

public class Square implements Polygon{
    @Override
    public int getNumberOfSides() {
        return 4;
    }
}
