package cz.cvut.od.model;

public class EntryGate implements Gate {

    @Override
    public void open(Person person) {
        System.out.println("entry gate was opened");
    }
}
