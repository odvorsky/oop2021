package cz.cvut.od.model;

import java.util.Arrays;

public class MyOwnIntegerList {

    private Integer[] internalArray = new Integer[50];
    private int actualIndex = 0;

    public Integer get(int i){
        return this.internalArray[i];
    }

    public void add(Integer item){
        this.internalArray[actualIndex] = item;
        actualIndex++;
    }

    @Override
    public String toString() {
        return "MyOwnIntegerList{" +
                "internalArray=" + Arrays.toString(internalArray) +
                ", actualIndex=" + actualIndex +
                '}';
    }
}
