package cz.cvut.od.model;

public class WrappableBirthdayPresent<TYPE extends Wrappable> {
    private TYPE item;

    public TYPE unwrap() {
        item.print();
        return item;
    }

    public void wrap(TYPE item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return "BirthdayPresent{" +
                "item=" + item +
                '}';
    }
}
