package cz.cvut.od.model;

public class ToyCar implements Wrappable {

    private String name;

    public ToyCar(String name) {
        this.name = name;
    }

    @Override
    public void print() {
        System.out.println(this.name);
    }
}
