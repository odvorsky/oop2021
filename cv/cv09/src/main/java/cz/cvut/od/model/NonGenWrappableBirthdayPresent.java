package cz.cvut.od.model;

public class NonGenWrappableBirthdayPresent{
    private Wrappable item;

    public Wrappable unwrap() {
        item.print();
        return item;
    }

    public void wrap(Wrappable item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return "BirthdayPresent{" +
                "item=" + item +
                '}';
    }
}
