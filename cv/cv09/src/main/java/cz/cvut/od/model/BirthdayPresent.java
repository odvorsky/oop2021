package cz.cvut.od.model;

public class BirthdayPresent<TYPE> {
    private TYPE item;

    public TYPE unwrap() {
        return item;
    }

    public void wrap(TYPE item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return "BirthdayPresent{" +
                "item=" + item +
                '}';
    }
}
