package cz.cvut.od.model;

import java.util.Arrays;

public class MyOwnUniversalList {

    private Object[] internalArray = new Object[50];
    private int actualIndex = 0;

    public Object get(int i){
        return this.internalArray[i];
    }

    public void add(Object item){
        this.internalArray[actualIndex] = item;
        actualIndex++;
    }

    @Override
    public String toString() {
        return "MyOwnUniversalList{" +
                "internalArray=" + Arrays.toString(internalArray) +
                ", actualIndex=" + actualIndex +
                '}';
    }
}
