package cz.cvut.od.model;

import java.util.Arrays;

public class MyOwnList {

    private String[] internalArray = new String[50];
    private int actualIndex = 0;

    public String get(int i){
        return this.internalArray[i];
    }

    public void add(String item){
        this.internalArray[actualIndex] = item;
        actualIndex++;
    }

    @Override
    public String toString() {
        return "MyOwnList{" +
                "internalArray=" + Arrays.toString(internalArray) +
                ", actualIndex=" + actualIndex +
                '}';
    }
}
