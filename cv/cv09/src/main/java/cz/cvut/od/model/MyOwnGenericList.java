package cz.cvut.od.model;

import java.util.Arrays;

public class MyOwnGenericList<TYPE> {

    private Object[] internalArray = new Object[50];
    private TYPE item;
    private int actualIndex = 0;

    public TYPE get(int i){
        return (TYPE) this.internalArray[i];
    }

    public void add(TYPE item){
        this.internalArray[actualIndex] = item;
        actualIndex++;
    }

    @Override
    public String toString() {
        return "MyOwnUniversalList{" +
                "internalArray=" + Arrays.toString(internalArray) +
                ", actualIndex=" + actualIndex +
                '}';
    }
}
