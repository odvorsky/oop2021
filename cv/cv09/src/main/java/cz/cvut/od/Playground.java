package cz.cvut.od;

import cz.cvut.od.model.*;

import java.util.ArrayList;
import java.util.List;

public class Playground {

    public void play() {
        List<String> stringList = new ArrayList<>();
        this.myOwnList();
        this.birthdayPresent();
        this.exampleTeamListCount();
    }

    public void exampleTeamListCount() {
        List<Team> teams = new ArrayList<>();
        teams.add(new Team("Praha"));
        teams.add(new Team("Olomouc"));
        teams.add(new Team("Plzeň"));

        System.out.println(countPragueTeams(teams));
    }

    public <TYPE extends Team> int countPragueTeamsGeneric(List<TYPE> teams, GenericTeamPredicate<TYPE> predicate) {
        int count = 0;
        for (TYPE team : teams) {
            if (predicate.apply(team)) {
                count++;
            }
        }
        return count;
    }

    public <TYPE extends Team> int countPragueTeams(List<TYPE> teams) {
        int count = 0;
        for (Team team : teams) {
            if (team.getHomeTown().equals("Praha")) {
                count++;
            }
        }
        return count;
    }

    public <TYPE extends Wrappable> void printWrappableListExample(List<TYPE> list) {
        for (Wrappable wrappable : list) {
            wrappable.print();
        }
    }

    public void objectBirthdayPresent() {
        ObjectBirthdayPresent objP = new ObjectBirthdayPresent();
        objP.wrap("auto");

        Object presentContent = objP.unwrap();
        String strContent = null;
        if (presentContent instanceof String) {
            strContent = (String) presentContent;
        }

        if (strContent.length() < 3) {
            System.out.println("content is less than 3 chars");
        }

        BirthdayPresent<String> strPresent = new BirthdayPresent<>();
        strPresent.wrap("auto");
        strContent = strPresent.unwrap();

        if (strContent.length() < 3) {
            System.out.println("content is less than 3 chars");
        }

    }

    public void wrappableBirthdayPresent() {
        ToyCar toyCar = new ToyCar("autíčko");
        WrappableBirthdayPresent<ToyCar> present = new WrappableBirthdayPresent<>();
        present.wrap(toyCar);

        //WrappableBirthdayPresent<String> stringWrappablePres = new WrappableBirthdayPresent<>();
    }

    public void birthdayPresent() {
        BirthdayPresent<String> stringPresent = new BirthdayPresent<>();
        stringPresent.wrap("AAA");
        System.out.println(stringPresent.unwrap());

        BirthdayPresent objectPresent = new BirthdayPresent();
        objectPresent.wrap("AAA");
    }

    public void myOwnList() {
        MyOwnList list = new MyOwnList();
        list.add("A");
        list.add("B");
        System.out.println(list);
        System.out.println(list.get(0));

        MyOwnIntegerList intList = new MyOwnIntegerList();
        intList.add(1);
        intList.add(2);

        boolean b = true;
        Boolean b2 = true;
        Boolean b3 = null;
        String s = null;
        Integer i = null;

        MyOwnUniversalList universalList = new MyOwnUniversalList();
        universalList.add("Adam");
        universalList.add(1);

        MyOwnGenericList<String> myOwnGenericList = new MyOwnGenericList<>();
        myOwnGenericList.add("AAA");

        MyOwnGenericList<Integer> myOwnGenericListInt = new MyOwnGenericList<>();
        myOwnGenericListInt.add(1);
    }
}
