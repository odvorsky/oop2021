package cz.cvut.od.model;

public class ObjectBirthdayPresent {

    private Object present;

    public Object unwrap() {
        return present;
    }

    public void wrap(Object present) {
        this.present = present;
    }
}
