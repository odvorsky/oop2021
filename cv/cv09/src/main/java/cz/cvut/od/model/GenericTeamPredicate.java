package cz.cvut.od.model;

public interface GenericTeamPredicate<TYPE extends Team> {

    boolean apply(TYPE type);
}
