package cz.cvut.od.model;

import java.time.LocalDateTime;

public class Event {
    private EventType type;
    private LocalDateTime eventDateTime = LocalDateTime.now();

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public LocalDateTime getEventDateTime() {
        return eventDateTime;
    }

    @Override
    public String toString() {
        return "Event{" +
                "type=" + type +
                ", čas=" + eventDateTime +
                '}';
    }
}
