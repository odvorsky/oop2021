package cz.cvut.od.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Championship {

    private List<Team> teams = new LinkedList<>();
    private List<Match> matches = new ArrayList<>();

    public void registerTeam(Team team) {
        teams.add(team);
    }

    public void generateMatches() {
        if (this.teams.size() < 2) {
            throw new IllegalStateException("less than two matches registered");
        }
        for (int i = 0; i < teams.size(); i++) {
            for (int j = i + 1; j < teams.size(); j++) {
                matches.add(new Match(teams.get(i), teams.get(j)));
            }
        }
    }

    public void simulateMatches() {
        for (Match ma : this.matches) {
            ma.simulateMatch();
        }
    }


    public List<Team> getTeams() {
        return teams;
    }

    public List<Match> getMatches() {
        return matches;
    }
}
