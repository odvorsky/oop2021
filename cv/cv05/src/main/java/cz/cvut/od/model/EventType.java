package cz.cvut.od.model;

public enum EventType {
    START,
    END,
    GOAL
}
