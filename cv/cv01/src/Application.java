public class Application {

    public static void main(String[] args) {
        int x;
        int y;
        int v;

        x = 5;
        y = 5;

        v = x + y;

        if( (v > 1 && v < 30 && v != 1 && v == 10) || !false || false ){
            System.out.println("Výraz platí.");
        } else if(v > 10){
            System.out.println("Druhý výraz platí.");
        } else {
            System.out.println("Neplatí žádný výraz.");
        }

        String resultText = "Výsledek je: ";
        System.out.println(resultText + v);

        int vysledek = 0;
        int[] firstArray = { 1, 2, 3, 4, 5 };
        for(int value: firstArray){
            vysledek = vysledek + value; // alternativní zápis: vysledek += value;
        }



        int[] secondArray = new int[50];
        secondArray[0] = 1;
        secondArray[49] = 2;
        System.out.println(secondArray[0]);
        System.out.println(secondArray.length);
    }
}
