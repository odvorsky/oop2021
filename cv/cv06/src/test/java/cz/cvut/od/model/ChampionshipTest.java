package cz.cvut.od.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class ChampionshipTest {

    @Test
    public void BADgenerateMatches_generatedMatchesCountMoreThan1_returnCount(){
        Championship championship = new Championship();
        int result = championship.generateMatches();
        Assertions.assertEquals(15, result);
    }

    @Test
    public void generateMatches_generatedMatchesCountMoreThan1_returnCount(){
        Championship championship = new Championship();
        MatchGenerator mockGenerator = Mockito.mock(MatchGenerator.class);
        Mockito.when(mockGenerator.getMatches()).thenReturn(5);

        championship.setMatchGenerator(mockGenerator);

        int result = championship.generateMatches();
        Assertions.assertEquals(5, result);
    }

    @Test
    public void generateMatches_generatedMatchesCountLessThan1_throwExc(){
        Championship championship = new Championship();

        MatchGenerator mockGenerator = Mockito.mock(MatchGenerator.class);
        Mockito.when(mockGenerator.getMatches()).thenReturn(0);

        championship.setMatchGenerator(mockGenerator);

        Assertions.assertThrows(IllegalStateException.class, () -> championship.generateMatches());
    }
}
