package cz.cvut.od;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class DashboardTest {


    @Test
    public void getPrintableBalance_always_appendCurrencyInfo(){
        Dashboard dashboard = new Dashboard(new MockAccountBalanceService());
        Assertions.assertTrue(dashboard.getPrintableBalance().contains(",- Kč"));
    }

    @Test
    public void getPrintableBalanceMockito_always_appendCurrencyInfo(){

        AccountBalanceService mock = Mockito.mock(AccountBalanceService.class);
        Mockito.when(mock.getBalance()).thenReturn("50");
        Dashboard dashboard = new Dashboard(mock);

        System.out.println(dashboard.getPrintableBalance());

    }

    @Test
    public void newBalance_balance1000_printNotEnoughFunds(){
        AccountBalanceService mock = Mockito.mock(AccountBalanceService.class);
        Mockito.when(mock.getBalance()).thenReturn("1000");
        Dashboard dashboard = new Dashboard(mock);
        Assertions.assertEquals("Nedostatečný zůstatek.", dashboard.getNewBalance());
    }
}
