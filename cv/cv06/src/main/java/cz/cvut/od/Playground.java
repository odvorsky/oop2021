package cz.cvut.od;

import java.util.*;

public class Playground {

    public void play(){
        //set();
        dashboard();
    }

    public void dashboard(){
        Dashboard dashboard = new Dashboard(new MockAccountBalanceService());
        dashboard.showBalance();
    }

    public void set(){
        Set<String> set = new HashSet<>();
        set.add("David");
        set.add("Adam");
        set.add("Franta");

        System.out.println(set);

        Set<String> set2 = new TreeSet<>();

        set2.add("David");
        set2.add("Adam");
        set2.add("Franta");

        System.out.println(set2);

        List<String> list = new ArrayList<>();
    }
}
