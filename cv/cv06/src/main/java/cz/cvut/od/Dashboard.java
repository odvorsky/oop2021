package cz.cvut.od;

public class Dashboard {

    private AccountBalanceService accountBalanceService;

    public Dashboard(AccountBalanceService accountBalanceService) {
        this.accountBalanceService = accountBalanceService;
    }

    public String getPrintableBalance() {
        return accountBalanceService.getBalance() + ",- Kč";
    }

    public String getNewBalance() {
        String result = accountBalanceService.getBalance();
        if (result.equals("1000")) {
            return "Nedostatečný zůstatek.";
        }
        return result;
    }

    public void showBalance() {
        System.out.println(getPrintableBalance());
    }
}
