package cz.cvut.od.model;

public class Championship {
    private MatchGenerator matchGenerator = new MatchGenerator();

    public int generateMatches(){
        int result = matchGenerator.getMatches();
        if(result < 1){
            throw new IllegalStateException();
        }
        return result;
    }

    public void setMatchGenerator(MatchGenerator matchGenerator) {
        this.matchGenerator = matchGenerator;
    }
}
