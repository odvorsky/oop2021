package cz.cvut.od;

public interface AccountBalanceService {

    String getBalance();
}
