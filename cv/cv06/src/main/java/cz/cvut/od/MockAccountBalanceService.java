package cz.cvut.od;

public class MockAccountBalanceService implements AccountBalanceService{

    @Override
    public String getBalance() {
        return "1 000 000";
    }
}
