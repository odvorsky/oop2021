package cz.cvut.od;

import cz.cvut.od.model.Calculator;
import cz.cvut.od.model.Patient;
import cz.cvut.od.model.StatefulCalculator;

public class Playground {

    public Playground(){
        System.out.println("new Playground was created");
    }

    public void play(){
        System.out.println("play was called");

        Patient p1 = new Patient("1", 12);

        System.out.println(p1.getPersonalId());
        p1.setPersonalId("9");
        System.out.println(p1.getPersonalId());
        System.out.println();

        Patient p2 = new Patient();
        p2.setPersonalId("2222");

        Calculator calculator = new Calculator();
        int result = calculator.plus(1, 1);
        System.out.println(result);
        result = calculator.plus(10, 10);
        System.out.println(result);

        StatefulCalculator stf = new StatefulCalculator(1, 1);
        stf.plus();

        StatefulCalculator stf2 = new StatefulCalculator(5, 5);
        stf2.plus();

    }

}
