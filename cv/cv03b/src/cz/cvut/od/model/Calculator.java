package cz.cvut.od.model;

public class Calculator {

    public int plus(int a, int b){
        return a + b;
    }

    public int plus(int a, int b, int c){
        return a + b + c;
    }

}
