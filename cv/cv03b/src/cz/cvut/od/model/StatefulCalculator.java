package cz.cvut.od.model;

public class StatefulCalculator {
    private int x;
    private int y;

    public StatefulCalculator(int a, int b){
        x = a;
        y = b;
    }

    public int plus(){
        return x + y;
    }

}
