package cz.cvut.od.model;

public class Voter extends Person {

    private double weight;

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void elect(Politician politician){
        politician.elected();
    }
}
