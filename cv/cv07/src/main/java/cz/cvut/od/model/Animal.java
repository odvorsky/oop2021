package cz.cvut.od.model;

public interface Animal {

    void makeSound();

}
