package cz.cvut.od;

import cz.cvut.od.model.*;

public class Application {

    public static void main(String[] args) {

   /*     Duck duck = new Duck();
        Dog dog = new Dog();
        Animal duck2 = new Duck();
        Animal dog2 = new Dog();

        List<Animal> animals = new ArrayList<>();
        animals.add(duck);
        animals.add(dog2);

        for(Animal animal: animals){
            animal.makeSound();
        }
*/
        Politician jp = new Politician();
        jp.setFirstName("Jiří");
        jp.setLastName("Paroubek");

        Politician lb = new Politician();
        lb.setFirstName("Lubomír");
        lb.setLastName("Volný");

        Party cssd = new Party("CSSD");
        cssd.registerPolitician(jp);

        Party vb = new Party("Volný blok");
        vb.registerPolitician(lb);

        Election psElection2021 = new Election();
        psElection2021.setType("Volby do poslenecké sněmovny");
        psElection2021.setYear(2021);
        psElection2021.setWinnerCalculator(new SimpleElectionWinnerCalculator());

        psElection2021.registerParty(cssd);
        psElection2021.registerParty(vb);

        Voter mo = new Voter();
        mo.setFirstName("Milena");
        mo.setLastName("Otrubová");
        mo.setWeight(1.0);

        mo.elect(jp);
        mo.elect(jp);
        mo.elect(lb);

        System.out.println(psElection2021);

        System.out.println("Winner is: " + psElection2021.getWinner().getName());

    }
}
