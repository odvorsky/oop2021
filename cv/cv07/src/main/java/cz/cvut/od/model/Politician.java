package cz.cvut.od.model;

public class Politician extends Person {
    private int voteCount = 0;

    public void elected(){
        this.voteCount++;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public String toString() {
        return "Politician{" +
                "firstName='" + getFirstName() + '\'' +
                ", lastName='" + getLastName() + '\'' +
                ", voteCount='" + this.voteCount + '\'' +
                '}';
    }
}
