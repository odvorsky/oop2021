package cz.cvut.od.model;

import java.util.ArrayList;
import java.util.List;

public class Election {
    private int year;
    private String type;
    private List<Party> parties = new ArrayList<>();
    private WinnerCalculator winnerCalculator;

    public void registerParty(Party party) {
        this.parties.add(party);
    }

    public Party getWinner() {
        return winnerCalculator.getWinner(this.parties);
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public WinnerCalculator getWinnerCalculator() {
        return winnerCalculator;
    }

    public void setWinnerCalculator(WinnerCalculator winnerCalculator) {
        this.winnerCalculator = winnerCalculator;
    }

    @Override
    public String toString() {
        return "Election{" +
                "year=" + year +
                ", type='" + type + '\'' +
                ", parties=" + parties +
                '}';
    }
}
