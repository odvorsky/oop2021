package cz.cvut.od.model;

import java.util.List;

public interface WinnerCalculator {

    Party getWinner(List<Party> parties);
}
