package cz.cvut.od.model;

import java.util.List;

public class SimpleElectionWinnerCalculator implements WinnerCalculator {

    @Override
    public Party getWinner(List<Party> parties) {
        Party winner = null;
        int maxVoteCount = 0;
        for (Party party : parties) {
            int voteCount = 0;
            for (Politician politician : party.getPoliticians()) {
                voteCount += politician.getVoteCount();
            }
            if(maxVoteCount < voteCount){
                maxVoteCount = voteCount;
                winner = party;
            }
        }
        return winner;
    }
}
