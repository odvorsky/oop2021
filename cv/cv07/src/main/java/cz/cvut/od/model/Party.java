package cz.cvut.od.model;

import java.util.ArrayList;
import java.util.List;

public class Party {
    private String name;
    private List<Politician> politicians = new ArrayList<>();

    public Party(String name) {
        this.name = name;
    }

    public void registerPolitician(Politician politician){
        this.politicians.add(politician);
    }

    public String getName() {
        return name;
    }

    public List<Politician> getPoliticians() {
        return politicians;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Party{" +
                "name='" + name + '\'' +
                ",politicians=" + politicians +
                '}';
    }
}
