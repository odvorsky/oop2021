package cz.cvut.od.model;

public class Duck implements Animal{

    @Override
    public void makeSound() {
        System.out.println("QUACK");
    }
}
