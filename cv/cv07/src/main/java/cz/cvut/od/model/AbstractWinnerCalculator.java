package cz.cvut.od.model;

import java.util.List;

public abstract class AbstractWinnerCalculator {

    public Party getWinnerAndLog(List<Party> parties){
        Party winner = getWinner(parties);
        System.out.println(winner.getName());
        return winner;
    }

    abstract Party getWinner(List<Party> parties);

}
